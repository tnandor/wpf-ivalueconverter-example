﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LassukConverter
{
   public class ViewModel
    {
        public ObservableCollection<Book> Books { get; set; }

        public ViewModel()
        {
            ObservableCollection<Book> newBooks = new ObservableCollection<Book>();

            newBooks.Add(new Book("Hemingway", "The old man and the sea", BookType.Novel));
            newBooks.Add(new Book("ASUS", "XY Motherboard Users Manual", BookType.Manual));
            newBooks.Add(new Book("Stephen Hawking","A brief history of time",BookType.TextBook));
            newBooks.Add(new Book("Jiří Hájíček", "Green Horse Rustlers", BookType.Novel));

            Books = newBooks;
        }
    }
}
