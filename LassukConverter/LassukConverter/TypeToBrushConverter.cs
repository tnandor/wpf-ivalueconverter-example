﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace LassukConverter
{
    class TypeToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BookType t = (BookType)value;

            switch (t)
            {
                case BookType.Novel:
                    return Brushes.Yellow;                    
                case BookType.TextBook:
                    return Brushes.Green;                    
                case BookType.Manual:
                    return Brushes.Red;                  
             }

            return null;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
