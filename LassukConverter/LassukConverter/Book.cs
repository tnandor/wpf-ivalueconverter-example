﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LassukConverter
{
    public class Book
    {
        public string Author {get; set; }
        public string Title { get; set; }
        public BookType Type { get; set; }

        public Book(string auth,string titl, BookType t)
        {
            Author = auth;
            Title = titl;
            Type = t;
        }

    }

    public enum BookType
    {
        Novel,TextBook,Manual
    }
}
